![BBVA Continental](https://www.bbva.pe/content/dam/public-web/peru/images/logos/logo-bbva.png)

# ApiRestTechU

_Api REST final practitioner_

Tech U

### Detalles

* Ramas: master, develop y feature/user_module
* Proyecto Node JS V12.4.0
* Express 4.17.0
* URL HEROKU: https://bbva-tech-u.herokuapp.com/

### Instalación en ambiente local

1. Clonar el repositorio
2. Modificar el archivo .env en caso cambiar varias de ambiente
3. Instalar módulos de Node

```
npm install
```
4. Inicial api
```
node index.js
```